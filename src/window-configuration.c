#include <stdlib.h>
#include <glib-object.h>

#include "window-configuration.h"

gboolean
simple_compositor_window_size_from_string (char              *value,
                                           WindowSize        *size,
                                           GError           **error)
{
  if (g_str_has_suffix (value, "%"))
    {
      size->is_percentage = TRUE;
      size->percentage = g_ascii_strtod (value, NULL);
      return TRUE;
    }
  else if (g_str_has_suffix (value, "px"))
    {
      size->pixels = g_ascii_strtoll (value, NULL, 10);
      size->is_percentage = FALSE;
      return TRUE;
    }
  else
    {
      *error = g_error_new (g_key_file_error_quark (),
                            G_KEY_FILE_ERROR_INVALID_VALUE,
                            "Failed to parse window size value %s", value);
      return FALSE;
    }
}

void
simple_compositor_window_size_compute (WindowConfiguration *config,
                                       int                  monitor_width,
                                       int                  monitor_height,
                                       int                 *width,
                                       int                 *height)
{
  if (config->width.is_percentage)
    {
      *width = monitor_width * config->width.percentage / 100.0;
    }
  else
    {
      if (monitor_width >= config->width.pixels)
        *width = config->width.pixels;
      else
        *width = monitor_width;
    }

  if (config->height.is_percentage)
    {
      *height = monitor_height * config->height.percentage / 100.0;
    }
  else
    {
      if (monitor_height >= config->height.pixels)
        *height = config->height.pixels;
      else
        *height = monitor_height;
    }
}

void
simple_compositor_window_position_compute (WindowConfiguration *config,
                                           GPtrArray           *state,
                                           int                  computed_w,
                                           int                  computed_h,
                                           int                  monitor_width,
                                           int                  monitor_height,
                                           int                 *x0,
                                           int                 *y0)
{
  int before_w, before_h;
  int i;
  *x0 = 0;
  *y0 = 0;
  if (config->is_centered && config->position <= state->len)
    {
      // Compute the x0, y0 for the position we are centered on
      WindowConfiguration *centered_cfg = state->pdata[config->position];
      simple_compositor_window_size_compute (centered_cfg, monitor_width, monitor_height, &before_w, &before_h);
      *x0 = (before_w - computed_w) / 2.0;
      *y0 = (before_h - computed_h) / 2.0;
    }
  else
    {
      for (i = 0; i < state->len; i++)
        {
          WindowConfiguration *before_cfg = state->pdata[i];
          if (before_cfg->position < config->position && !before_cfg->is_centered && !config->is_centered)
            {
              simple_compositor_window_size_compute (before_cfg, monitor_width, monitor_height, &before_w, &before_h);
              if (before_h <= monitor_height && *y0 <= monitor_height)
                *y0 += before_h;
            }
        }
    }
  *x0 = CLAMP (*x0, 0, monitor_width);
  *y0 = CLAMP (*y0, 0, monitor_height);
}

static void
window_configuration_free (void *user_data)
{
  WindowConfiguration *self = user_data;
  g_free (self->identifier);
  g_clear_object (&self->window);
  if (self->app_id != NULL)
    g_free (self->app_id);
  if (self->window_object_path != NULL)
    g_free (self->window_object_path);
  if (self->window_title != NULL)
    g_free (self->window_title);
  g_free (self);
}

void
simple_compositor_window_configuration_debug (WindowConfiguration *self)
{
  GString *debug = g_string_new ("WindowConfiguration");
  g_string_append_printf (debug, "(identifier = %s, ", self->identifier);
  if (self->window_title)
    g_string_append_printf (debug, "title = %s, ", self->window_title);
  if (self->window_object_path)
    g_string_append_printf (debug, "window-object-path = %s, ", self->window_object_path);
  if (self->monitor)
    g_string_append_printf (debug, "monitor = %d, ", self->monitor);
  g_string_append_printf (debug, "x0 = %d, ", self->x0);
  g_string_append_printf (debug, "y0 = %d, ", self->y0);
  g_string_append_printf (debug, "is_centered = %d, ", self->is_centered);
  g_string_append_printf (debug, "is_fullscreened = %d, ", self->is_fullscreened);
  g_string_append_printf (debug, "is_always_on_top= %d, ", self->is_always_on_top);
  if (self->app_id)
    g_string_append_printf (debug, "app-id = %s, ", self->app_id);
  if (self->width.is_percentage)
    g_string_append_printf (debug, "width = %f%%, ", self->width.percentage);
  else
    g_string_append_printf (debug, "width = %ldpx, ", self->width.pixels);
  if (self->height.is_percentage)
    g_string_append_printf (debug, "height = %f%%, ", self->height.percentage);
  else
    g_string_append_printf (debug, "height = %ldpx, ", self->height.pixels);
  g_string_append_printf (debug, "position = %d", self->position);
  g_string_append (debug, ")");

  g_warning ("%s", debug->str);
  g_string_free (debug, TRUE);
}

GPtrArray *
simple_compositor_parse_configuration (const gchar  *file,
                                       GError      **error)
{
  g_autoptr (GPtrArray) state = NULL;
  g_autoptr (GKeyFile) config = NULL;
  const gchar**groups;
  const gchar**keys;
  gsize n_groups, i, j, n_keys;

  config = g_key_file_new ();

  if (!g_key_file_load_from_file (config, file, G_KEY_FILE_NONE, error))
    return NULL;

  groups = g_key_file_get_groups (config, &n_groups);

  if (n_groups == 0)
    {
      g_debug ("Found 0 groups in the configuration file");
      return NULL;
    }

  state = g_ptr_array_new_full (n_groups, window_configuration_free);

  for (i = 0; i < n_groups; i++)
    {
      WindowConfiguration *window = g_new0 (WindowConfiguration, 1);
      window->is_centered = FALSE;
      window->is_fullscreened = FALSE;
      window->is_always_on_top = FALSE;
      window->monitor = 0;
      window->x0 = -1;
      window->y0 = -1;
      window->computed_x0 = 0;
      window->computed_y0 = 0;
      window->computed_height = 0;
      window->computed_width = 0;

      window->identifier = g_strdup (groups[i]);
      keys = g_key_file_get_keys (config, groups[i], &n_keys, error);
      g_return_val_if_fail (error == NULL || *error == NULL, NULL);

      for (j = 0; j < n_keys; j++)
        {
          if (g_strcmp0 (keys[j],  "width") == 0 || g_strcmp0 (keys[j],  "height") == 0)
            {
              g_autofree gchar *value = g_key_file_get_string (config, groups[i], keys[j], error);
              WindowSize size;

              if (!simple_compositor_window_size_from_string (value, &size, error))
                return NULL;

              if (g_strcmp0 (keys[j],  "width") == 0)
                window->width = size;
              else
                window->height = size;
            }
          else if (g_strcmp0 (keys[j], "position") == 0)
            {
              int position = g_key_file_get_integer (config, groups[i], keys[j], error);
              g_return_val_if_fail (error == NULL || *error == NULL, NULL);
              window->position = position;
            }
          else if (g_strcmp0 (keys[j], "x0") == 0)
            {
              int x0 = g_key_file_get_integer (config, groups[i], keys[j], error);
              g_return_val_if_fail (error == NULL || *error == NULL, NULL);
              window->x0 = x0;
            }
          else if (g_strcmp0 (keys[j], "y0") == 0)
            {
              int y0 = g_key_file_get_integer (config, groups[i], keys[j], error);
              g_return_val_if_fail (error == NULL || *error == NULL, NULL);
              window->y0 = y0;
            }
          else if (g_strcmp0 (keys[j], "centered") == 0)
            {
              gboolean centered = g_key_file_get_boolean (config, groups[i], keys[j], error);
              g_return_val_if_fail (error == NULL || *error == NULL, NULL);
              window->is_centered = centered;
            }
          else if (g_strcmp0 (keys[j], "id") == 0)
            {
              g_autofree gchar *id = g_key_file_get_string (config, groups[i], keys[j], error);
              g_return_val_if_fail (error == NULL || *error == NULL, NULL);
              window->app_id = g_strdup (id);
            }
          else if (g_strcmp0 (keys[j], "title") == 0)
            {
              g_autofree gchar *title = g_key_file_get_string (config, groups[i], keys[j], error);
              g_return_val_if_fail (error == NULL || *error == NULL, NULL);
              window->window_title = g_strdup (title);
            }
          else if (g_strcmp0 (keys[j], "window-object-path") == 0)
            {
              g_autofree gchar *object_path = g_key_file_get_string (config, groups[i], keys[j], error);
              g_return_val_if_fail (error == NULL || *error == NULL, NULL);
              window->window_object_path = g_strdup (object_path);
            }
          else if (g_strcmp0 (keys[j], "monitor") == 0)
            {
              unsigned int monitor = g_key_file_get_integer (config, groups[i], keys[j], error);
              g_return_val_if_fail (error == NULL || *error == NULL, NULL);
              window->monitor = monitor;
            }
          else if (g_strcmp0 (keys[j], "fullscreen") == 0)
            {
              gboolean fullscreened = g_key_file_get_boolean (config, groups[i], keys[j], error);
              g_return_val_if_fail (error == NULL || *error == NULL, NULL);
              window->is_fullscreened = fullscreened;
            }
          else if (g_strcmp0 (keys[j], "always-on-top") == 0)
          {
            gboolean always_on_top = g_key_file_get_boolean (config, groups[i], keys[j], error);
            g_return_val_if_fail (error == NULL || *error == NULL, NULL);
            window->is_always_on_top = always_on_top;
          }
        }

      g_ptr_array_add (state, window);
    }

  return g_steal_pointer (&state);
}
