#include <glib.h>

typedef struct _WindowSize {
  gboolean is_percentage;
  double percentage;
  gint64 pixels;
} WindowSize;

typedef struct _WindowConfiguration
{
  gpointer *window;
  char *identifier;
  char *app_id;
  char *window_object_path;
  char *window_title;
  gboolean is_fullscreened;
  gboolean is_always_on_top;
  unsigned int monitor;
  int position;
  gboolean is_centered;
  int x0;
  int y0;
  int computed_x0;
  int computed_y0;
  int computed_width;
  int computed_height;
  WindowSize width;
  WindowSize height;
} WindowConfiguration;


GPtrArray *
simple_compositor_parse_configuration (const gchar  *file,
                                       GError      **error);

void
simple_compositor_window_configuration_debug (WindowConfiguration *self);

void
simple_compositor_window_size_compute (WindowConfiguration *config,
                                       int                  monitor_width,
                                       int                  monitor_height,
                                       int                 *width,
                                       int                 *height);
void
simple_compositor_window_position_compute (WindowConfiguration *config,
                                           GPtrArray           *state,
                                           int                  computed_w,
                                           int                  computed_h,
                                           int                  monitor_width,
                                           int                  monitor_height,
                                           int                 *x0,
                                           int                 *y0);

gboolean
simple_compositor_window_size_from_string (char              *value,
                                           WindowSize        *size,
                                           GError           **error);
