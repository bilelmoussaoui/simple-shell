#include <glib-object.h>
#include <meta/meta-plugin.h>

G_BEGIN_DECLS
#define SIMPLE_TYPE_COMPOSITOR (simple_compositor_get_type ())

G_DECLARE_FINAL_TYPE (SimpleCompositor,
                      simple_compositor,
                      SIMPLE,
                      COMPOSITOR,
                      MetaPlugin)
G_END_DECLS
