#include "../window-configuration.h"


#define WINDOW_SIZE_PERCENTAGE(_percentage) \
        (WindowSize) { \
          .is_percentage = TRUE, \
          .percentage = (_percentage), \
        }

static void
parse_window_size (void)
{
  gboolean success;
  WindowSize size;
  g_autoptr (GError) error = NULL;

  simple_compositor_window_size_from_string (g_strdup ("100px"), &size, &error);
  g_assert (!size.is_percentage);
  g_assert_cmpint (size.pixels, ==, 100);

  simple_compositor_window_size_from_string (g_strdup ("30%"), &size, &error);
  g_assert (size.is_percentage);
  g_assert_cmpfloat (size.percentage, ==, 30.0);

  simple_compositor_window_size_from_string (g_strdup ("azeaze%"), &size, &error);
  g_assert (size.is_percentage);
  g_assert_cmpfloat (size.percentage, ==, 0.0);

  success = simple_compositor_window_size_from_string (g_strdup ("azezaeaz"),
                                                       &size, &error);
  g_assert (g_strcmp0 (error->message, "Failed to parse window size value azezaeaz") == 0);
  g_assert (!success);
}

static void
parse_valid_config (void)
{
  GPtrArray *state;
  WindowConfiguration *config;

  state = simple_compositor_parse_configuration ("../src/tests/test1.ini", NULL);
  g_assert_cmpint (state->len, ==, 5);

  config = state->pdata[0];
  g_assert_cmpint (g_strcmp0 (config->identifier, "other_app"), ==, 0);
  g_assert_cmpint (g_strcmp0 (config->app_id, "org.gnome.TextEditor"), ==, 0);
  g_assert_cmpint (config->position, ==, 0);
  g_assert (!config->is_centered);
  g_assert (config->width.is_percentage);
  g_assert_cmpfloat (config->width.percentage, ==, 100.0);
  g_assert (config->height.is_percentage);
  g_assert_cmpfloat (config->height.percentage, ==, 45.0);

  config = state->pdata[1];
  g_assert_cmpint (g_strcmp0 (config->identifier, "primary"), ==, 0);
  g_assert_cmpint (g_strcmp0 (config->app_id, "org.gnome.Nautilus"), ==, 0);
  g_assert_cmpint (config->position, ==, 1);
  g_assert (!config->is_centered);
  g_assert (config->width.is_percentage);
  g_assert_cmpfloat (config->width.percentage, ==, 100.0);
  g_assert (config->height.is_percentage);
  g_assert_cmpfloat (config->height.percentage, ==, 45.0);

  config = state->pdata[2];
  g_assert_cmpint (g_strcmp0 (config->identifier, "secondary"), ==, 0);
  g_assert_cmpint (g_strcmp0 (config->app_id, "org.gnome.clocks"), ==, 0);
  g_assert_cmpint (config->position, ==, 0);
  g_assert (config->is_centered);
  g_assert (config->width.is_percentage);
  g_assert_cmpfloat (config->width.percentage, ==, 30.0);
  g_assert (config->height.is_percentage);
  g_debug ("%f", config->height.percentage);
  g_assert_cmpfloat (config->height.percentage, ==, 22.5);

  config = state->pdata[3];
  g_assert_cmpint (g_strcmp0 (config->identifier, "toolbox"), ==, 0);
  g_assert_cmpint (g_strcmp0 (config->app_id, "org.gnome.Terminal"), ==, 0);
  g_assert_cmpint (config->position, ==, 2);
  g_assert (!config->is_centered);
  g_assert (config->width.is_percentage);
  g_assert_cmpfloat (config->width.percentage, ==, 100.0);
  g_assert (config->height.is_percentage);
  g_assert_cmpfloat (config->height.percentage, ==, 10.0);

  config = state->pdata[4];
  g_assert_cmpint (g_strcmp0 (config->identifier, "random"), ==, 0);
  g_assert_cmpint (g_strcmp0 (config->app_id, "org.random.app"), ==, 0);
  g_assert_cmpint (config->monitor, ==, 2);
  g_assert (config->is_fullscreened);
}

static void
window_size_compute (void)
{
  int computed_width, computed_height;
  int monitor_width = 1920;
  int monitor_height = 1080;
  WindowConfiguration *config = g_new0 (WindowConfiguration, 1);

  config->width = WINDOW_SIZE_PERCENTAGE (100);
  config->height = WINDOW_SIZE_PERCENTAGE (45);
  simple_compositor_window_size_compute (config, monitor_width, monitor_height,
                                         &computed_width, &computed_height);
  g_assert_cmpfloat (computed_width, ==, 1920.0);
  g_assert_cmpfloat (computed_height, ==, 486.0);

  config->height = WINDOW_SIZE_PERCENTAGE (10);
  simple_compositor_window_size_compute (config, monitor_width, monitor_height,
                                         &computed_width, &computed_height);
  g_assert_cmpfloat (computed_height, ==, 108.0);
}

static void
window_position_compute (void)
{
  GPtrArray *state;
  WindowConfiguration *config;
  int computed_width, computed_height, computed_x0, computed_y0;
  int monitor_width = 1920;
  int monitor_height = 1080;

  state = simple_compositor_parse_configuration ("../src/tests/test1.ini", NULL);
  config = state->pdata[0];
  simple_compositor_window_size_compute (config, monitor_width, monitor_height,
                                         &computed_width, &computed_height);
  simple_compositor_window_position_compute (config, state, computed_width, computed_height,
                                             monitor_width, monitor_height,
                                             &computed_x0, &computed_y0);
  g_assert_cmpint (computed_x0, ==, 0.0);
  g_assert_cmpint (computed_y0, ==, 0.0);

  config = state->pdata[1];
  simple_compositor_window_size_compute (config, monitor_width, monitor_height,
                                         &computed_width, &computed_height);
  simple_compositor_window_position_compute (config, state, computed_width, computed_height,
                                             monitor_width, monitor_height,
                                             &computed_x0, &computed_y0);
  g_assert_cmpint (computed_x0, ==, 0.0);
  g_assert_cmpint (computed_y0, ==, 486.0);

  config = state->pdata[2];
  simple_compositor_window_size_compute (config, monitor_width, monitor_height,
                                         &computed_width, &computed_height);
  simple_compositor_window_position_compute (config, state, computed_width, computed_height,
                                             monitor_width, monitor_height,
                                             &computed_x0, &computed_y0);
  g_assert_cmpint (computed_x0, ==, 672.0);
  g_assert_cmpint (computed_y0, ==, 121.0);

  config = state->pdata[3];
  simple_compositor_window_size_compute (config, monitor_width, monitor_height,
                                         &computed_width, &computed_height);
  simple_compositor_window_position_compute (config, state, computed_width, computed_height,
                                             monitor_width, monitor_height,
                                             &computed_x0, &computed_y0);
  g_assert_cmpint (computed_x0, ==, 0.0);
  g_assert_cmpint (computed_y0, ==, 972.0);
}

int main (int   argc,
          char *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/config/parse/window_size", parse_window_size);
  g_test_add_func ("/config/parse/valid-file", parse_valid_config);
  g_test_add_func ("/config/window-size-compute", window_size_compute);
  g_test_add_func ("/config/window-position-compute", window_position_compute);


  return g_test_run ();
}
