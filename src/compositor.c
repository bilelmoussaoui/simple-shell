#include <meta/meta-background-actor.h>
#include <meta/meta-background-content.h>
#include <meta/meta-background-group.h>
#include <systemd/sd-daemon.h>

#include "config.h"
#include "compositor.h"
#include "window-configuration.h"

static const CoglColor bg_color = { 0x2e, 0x34, 0x36, 0xff };

struct _SimpleCompositor
{
  MetaPlugin parent;
  MtkRectangle *monitors;
  unsigned int n_monitors;
  GPtrArray *state;
};

G_DEFINE_TYPE (SimpleCompositor, simple_compositor, META_TYPE_PLUGIN)

static gboolean
register_systemd (gpointer user_data)
{
  g_debug ("Systemd notified as ready");
  sd_notify (TRUE, "READY=1");

  return G_SOURCE_REMOVE;
}

static gboolean
config_equal (WindowConfiguration *config,
              MetaWindow          *window)
{
  if ((config->app_id != NULL && g_strcmp0 (config->app_id, meta_window_get_gtk_application_id (window)) == 0)
      || (config->window_object_path != NULL && g_strcmp0 (config->window_object_path, meta_window_get_gtk_window_object_path (window)) ==0)
      || (config->window_title != NULL && g_strcmp0 (config->window_title, meta_window_get_title (window)) == 0))
    return TRUE;
  else
    return FALSE;
}

static WindowConfiguration*
simple_compositor_get_config_for_window (SimpleCompositor *self,
                                         MetaWindow       *window)
{
  uint index;
  if (g_ptr_array_find_with_equal_func (self->state, window, config_equal, &index))
    {
      return self->state->pdata[index];
    }
  return NULL;
}

static void
on_window_shown (MetaWindow       *window,
                 SimpleCompositor *self)
{
  uint i;
  WindowConfiguration *config;

  for (i = 0; i < self->state->len; i++)
    {
      config = self->state->pdata[i];

      if ((MetaWindow*)config->window == window)
        {
          meta_window_move_resize_frame(config->window, FALSE,
                                        config->computed_x0, config->computed_y0,
                                        config->computed_width, config->computed_height);
          meta_window_move_to_monitor(config->window, config->monitor);
        }

      if (config->is_always_on_top)
        meta_window_make_above (config->window);
      else if (meta_window_is_above(config->window))
        meta_window_unmake_above (config->window);
    }
}

static void
on_window_pre_configured (MetaWindow       *window,
                          SimpleCompositor *self)
{
  int w, h, x0, y0 = 0;
  int monitor_width;
  int monitor_height;
  MetaDisplay *display;
  WindowConfiguration *config;

  display = meta_window_get_display (window);
  config = simple_compositor_get_config_for_window (self, window);

  if (config != NULL)
    {
      g_debug ("Found a configuration for window title=%s", meta_window_get_title (window));
      config->window = window;

      simple_compositor_window_configuration_debug (config);
      if (self->n_monitors > config->monitor)
        {
          monitor_width = self->monitors[config->monitor].width;
          monitor_height = self->monitors[config->monitor].height;
        }
      else
        {
          config->monitor = 0;
          monitor_width = self->monitors[0].width;
          monitor_height = self->monitors[0].height;
        }

      if (!config->is_fullscreened)
        {
          simple_compositor_window_size_compute (config, monitor_width, monitor_height, &w, &h);
          if (config->x0 == -1 || config->y0 == -1)
            {
              simple_compositor_window_position_compute (config, self->state,
                                                         monitor_width, monitor_height,
                                                         w, h, &x0, &y0);
            }
          else
            {
              x0 = config->x0;
              y0 = config->y0;
            }

          config->computed_x0 = x0;
          config->computed_y0 = y0;
          config->computed_width = w;
          config->computed_height = h;
        }
      else
        {
          config->computed_x0 = 0;
          config->computed_y0 = 0;
          config->computed_width = monitor_width;
          config->computed_height = monitor_height;
        }

      g_signal_connect (window,
                        "shown",
                        G_CALLBACK (on_window_shown),
                        self);
    }
  else
    {
      g_warning ("Didn't found a configuration for id=%s", meta_window_get_gtk_application_id (window));
    }
}

static void
on_window_created (MetaDisplay      *display,
                   MetaWindow       *window,
                   SimpleCompositor *self)
{
  g_signal_connect (window,
                    "pre-configured",
                    G_CALLBACK (on_window_pre_configured),
                    self);
}

static void
simple_compositor_start (MetaPlugin *plugin)
{
  ClutterStage *stage;
  int i;
  MtkRectangle geometry;
  ClutterActor *group, *bg_actor;
  MetaBackground *bg;
  const gchar *bg_file_path;

  SimpleCompositor *self = SIMPLE_COMPOSITOR (plugin);
  MetaDisplay *display = meta_plugin_get_display (plugin);
  group = meta_background_group_new ();

  self->n_monitors = meta_display_get_n_monitors (display);
  self->monitors = g_new0 (MtkRectangle, self->n_monitors);

  bg = meta_background_new (display);
  meta_background_set_color (bg, &bg_color);
  for (i = 0; i < self->n_monitors; i++)
    {
      meta_display_get_monitor_geometry (display, i, &geometry);
      self->monitors[i] = geometry;

      bg_actor = meta_background_actor_new (display, i);

      meta_background_content_set_background (
        clutter_actor_get_content (bg_actor),
        bg);

      clutter_actor_add_child (group, bg_actor);
      clutter_actor_set_position (bg_actor, geometry.x, geometry.y);
    }

  bg_file_path = g_getenv ("SIMPLE_BACKGROUND_FILE");
  if (bg_file_path != NULL) {
    GFile *file = g_file_new_for_path(bg_file_path);
    if (g_file_query_exists(file, NULL)) {
      meta_background_set_file(bg, file, G_DESKTOP_BACKGROUND_STYLE_STRETCHED);
    }
  }

  stage = meta_get_stage_for_display (display);
  clutter_actor_insert_child_below (stage, group, NULL);
  clutter_actor_show (stage);


  g_signal_connect (display,
                    "window-created",
                    G_CALLBACK (on_window_created),
                    self);

  g_idle_add (G_SOURCE_FUNC (register_systemd), NULL);
}

static void
simple_compositor_dispose (GObject *object)
{
  SimpleCompositor *self = SIMPLE_COMPOSITOR (object);

  g_ptr_array_free (&self->state, TRUE);
  g_free (&self->monitors);

  G_OBJECT_CLASS (simple_compositor_parent_class)->dispose (object);
}

static void
simple_compositor_class_init (SimpleCompositorClass *compositor_class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (compositor_class);
  MetaPluginClass *plugin_class = META_PLUGIN_CLASS (compositor_class);

  object_class->dispose = simple_compositor_dispose;

  plugin_class->start = simple_compositor_start;
}

static void
simple_compositor_init (SimpleCompositor *self)
{
  GPtrArray *state = NULL;
  g_autoptr (GError) error = NULL;
  const gchar*config_file = g_getenv ("SIMPLE_CONFIG");
  if (config_file == NULL)
    config_file = CONFIG_FILE;

  g_debug ("SimpleCompositor: Initializing with %s", config_file);

  state = simple_compositor_parse_configuration (config_file, &error);
  if (error != NULL)
    g_warning ("Failed to parse config file %s", error->message);
  else
    self->state = state;
}
