# Simple Shell

A very basic Shell built on top of libmutter that does nothing but start Mutter, sets a background color/image and calls systemd-notify so other services can depend on the simple-shell to be running.

Heavily inspired from [Gala](https://github.com/elementary/gala/) & [gnome-kiosk](https://gitlab.gnome.org/GNOME/gnome-kiosk)

## Configuration

A `config.ini` file is supported for configuring the windows initial positioning, size and so on.

The config file must be valid and installed at `$prefix/$datadir/simple-shell` and can be configured at runtime by using

```shell
SIMPLE_CONFIG=/home/user/config.ini simple-shell
```

Examples:

```ini
[toolbox]
id = org.gnome.Terminal
width = 100%
height = 10%
position = 0
```

```ini
[browser]
id = org.gnome.Web
monitor = 2
fullscreen = 1
```

More examples can be found in `data/config.ini` and `src/tests/*.ini`

### Available options

- `title`: Used to detect the window by it title
- `id`: Used to detect the window by it application ID
- `window-object-path`: Used to detect the window by it GTK DBus object path
- `width` / `height`: The width and height of the window, in either percentage or pixels
- `x0` / `y0`: The initial position of the window, defaults to -1 and falls back to computing the position based on the `position` attribute
- `fullscreen`: Whether the window has to be fullscreened
- `centered`: Whether the window is centered on top of another one
- `monitor`: The monitor where the window should be displayed
- `position`: The order of the window on y-axis, used to compute the window position and size
based on the surrounding windows
- `always-on-top`: Automatically put the window above every other windows on the same monitor

## Installation

### Install the required dependencies

- mutter-devel
- systemd-devel
- systemd-libs

### Create required user/group

```shell
groupadd -r demo
useradd -m -r -g demo -c "Default session for demo" demo
passwd -d demo
```

### Build

```shell
meson _build --prefix=/usr -Duser=demo
ninja -C _build
sudo ninja -C _build install
```

- The `user` option allows you to set which user the session should start as. A password-less user as we don't go through GDM for login.

### Enable systemd services

```shell
systemctl enable session.service
systemctl enable --user simple-shell.service
systemctl enable --user simple-shell.socket
```

### Remove GDM / GNOME Shell

```shell
sudo dnf remove --setopt protected_packages= gnome-shell
sudo dnf remove gdm gnome-session
```
